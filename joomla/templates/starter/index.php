<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.starter
 * 
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();

JHtml::_('bootstrap.framework');
// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/starter.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/font-awesome/css/font-awesome.min.css');

$this->setMetaData('generator','');
$menu = $app->getMenu();
$is_home = false;
if ($menu->getActive() == $menu->getDefault()) {
	$is_home = true;
}

?>
<!DOCTYPE html>
<html lang="de">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<jdoc:include type="head" />  <meta name="google-site-verification" content="BZkgUlx9PrOx54KAxIE_H86o6iowIPNeAO4_F6s4fc8" />
  <meta name="geo.region" content="DE-BW" />
  <meta name="geo.placename" content="Karlsruhe" />
  <meta name="geo.position" content="49.0031693;8.4043959" />
  <meta name="icbm" content="49.0031693, 8.4043959" />
  <meta name="designer" content="GU KOMMUNIKATION" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" data-offset="181">
    <!-- Navigation -->
<?php if ($is_home) : ?>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top"><jdoc:include type="modules" name="brand" /></a>
            </div>
            <div class="collapse navbar-collapse navbar-left navbar-main-collapse">
                <jdoc:include type="modules" name="top-nav" />
                <div class="mobile-nav footer">
                    <div class="telnumber"><div class="icon"><img src="/images/telefon.svg"></div><a href="tel:004972437297971">+49 7243 729 7971</a></div>
                    <div class="navlinks"><div class="left"><a href="/impressum">Impressum</a></div><div href="" class="right"><a href="/datenschutzerklaerung">Datenschutz</a></div></div>
                </div>
            </div>
            <!-- /.navbar-collapse -->
<?php else : ?>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand gray" href="/"><jdoc:include type="modules" name="brand" /></a>
            </div>
<?php endif; ?>
        </div>
        <!-- /.container -->
    </nav>
<?php if ($this->countModules('header')) : ?>
    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2"><jdoc:include type="modules" name="header" />
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php endif; ?>
<!-- <jdoc:include type="message" /> -->
<jdoc:include type="component" />
<?php if ($this->countModules('content1')) : ?>
    <!-- Content1 -->
    <section id="content1" class="container content-section">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2"><jdoc:include type="modules" name="content1" />
            </div>
        </div>
    </section>
    <div class="profilimage"></div>
<?php if ($this->countModules('content2')) : ?>
    <!-- Content6 -->
    <section id="content2" class="container content-section">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2"><jdoc:include type="modules" name="content2" />
            </div>
        </div>
    </section>
<?php endif; ?>           
<?php endif; ?>
<?php if ($this->countModules('content3')) : ?>
    <!-- Content2 -->
    <section class="content-section">
        <div id="content3" class="stretch-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2"><jdoc:include type="modules" name="content3" />
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
 
<?php if ($this->countModules('content4')) : ?>
    <!-- Content3 -->
    <section id="content4" class="container content-section">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2"><jdoc:include type="modules" name="content4" />
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if ($this->countModules('content5')) : ?>
    <!-- Content4 -->
    <section id="content5"><jdoc:include type="modules" name="content5" />
    </section>
<?php endif; ?>

<?php if ($this->countModules('content6')) : ?>
    <!-- Content2 -->
    <section id="content6" class="container content-section">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2"><jdoc:include type="modules" name="content6" />
            </div>
        </div>
    </section>
<?php endif; ?>
    <div class="profilimage kontakt"></div>
    
<?php if ($this->countModules('footer')) : ?>
    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-2"><jdoc:include type="modules" name="footer" />
                </div>
                <div class="col-lg-4 foo">
                    <jdoc:include type="modules" name="footer-nav" />
                </div>
            </div>
        </div>
    </footer>
<?php endif; ?>
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/jquery.easing.min.js"></script>
    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTj791w3UHLknsPzZ8lboSM5VMvYFfhiE"></script>
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/starter.js"></script>
</body>
</html>