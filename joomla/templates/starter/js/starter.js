/*!
 * JS
 * 
 * 
 */

// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if (jQuery(".navbar").offset().top > 220) {
        jQuery(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        jQuery(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
}

jQuery(window).scroll(collapseNavbar);
jQuery(document).ready(collapseNavbar);

// jQuery for page scrolling feature - requires jQuery Easing plugin
jQuery(function() {
    jQuery('a.page-scroll').bind('click', function(event) {
        var height = jQuery( window ).height();
        var addscroll = 180;
        
        if (height <= 460) {
            addscroll = 180;
        }
        var $anchor = jQuery(this);
        jQuery('html, body').stop().animate({
            scrollTop: jQuery($anchor.attr('href')).offset().top - addscroll
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
jQuery('.navbar-collapse ul li a').click(function() {
  if (jQuery(this).attr('class') != 'dropdown-toggle active' && jQuery(this).attr('class') != 'dropdown-toggle') {
    jQuery('.navbar-toggle:visible').click();
  }
});

// Google Maps Scripts
var map = null;
// When the window has finished loading create our google map below
google.maps.event.addDomListener(window, 'load', init);
google.maps.event.addDomListener(window, 'resize', function() {
    map.setCenter(new google.maps.LatLng(48.92304734, 8.47340518));
});

function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions

    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 15,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(48.92304734, 8.47340518), // Karlsruhe

        // Disables the default Google Maps UI components
        disableDefaultUI: false,
        scrollwheel: false,
        draggable: true,
		streetViewControl: false
    };

    // Get the HTML DOM element that will contain your map 
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using out element and options defined above
    map = new google.maps.Map(mapElement, mapOptions);

    var myLatLng = new google.maps.LatLng(48.92304734, 8.47340518);
	var image = '/images/map-marker.png';
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
		icon: image
    });

	var infowindow = new google.maps.InfoWindow({
		content:"Standort:<br> <strong>Praxis Reichenbacher</strong>"
	});

	google.maps.event.addListener(infowindow, 'domready', function() {
		var iwOuter = jQuery('.gm-style-iw');
		iwOuter.children('div').children('div').css({'color' : '#ffffff'});
		var iwBackground = iwOuter.prev();
		iwBackground.children(':nth-child(4)').css({'background-color' : '#e2cc00'});
		iwBackground.children(':nth-child(3)').find('div').children().css({'background-color' : '#e2cc00', 'box-shadow': 'none'});
		var iwCloseBtn = iwOuter.next();
		iwCloseBtn.css({});

	});

	// infowindow.open(map,marker);
	marker.addListener('click', function() {
		infowindow.open(map,marker);
	});
}

jQuery( ".navbar-toggle" ).click(function() {
    var height = jQuery( window ).height();
    var addscroll = 180;
        
    if (height <= 460) {
        var newheight;
        newheight = height - addscroll;
        jQuery("#page-top .navbar-fixed-top .navbar-collapse.navbar-left.navbar-main-collapse").css("max-height",newheight);
    }
});

jQuery( document ).ready(function() {
    console.log( "ready!" );
    var currentYear = (new Date).getFullYear();
    jQuery("footer .custom p").html("© "+currentYear+" <strong>PRAXIS REICHENBACHER</strong>");
});